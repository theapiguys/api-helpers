API Helpers: Harvest, Asana & Infusionsoft
==========================================

[![Latest Stable Version](https://img.shields.io/packagist/v/theapiguys/api-helpers.svg)](https://packagist.org/packages/theapiguys/api-helpers)

This package is originally a split from a project that works with those three
APIs. Its main purpose is to level the different ways they work, by providing
a common way to instantiate clients and retrieve data.

While it does include classes for the three SDKs, you do not need to have all
three installed to use its benefits. That's why there's no SDK on the composer's
`require` section - more on this in the install procedures below.

Installation
------------
1. Include this library in your project by requiring it:
`composer require theapiguys/api-helpers *`
2. Depending on the SDKs you need, you should also require the following
libraries:
    - Harvest: `cogitools/hapi: ^0.1.18`
    - Asana: `asana/asana: ^0.5.0`
    - Infusionsoft: `infusionsoft/php-sdk: 1.3.*`
3. Configure the following constants somewhere in your code, depending on the
SDKs you need:
    - `BASE_URL` **(mandatory)**: used to define the auth direct URI;
    - Auth parameters:
        - `HARVEST_ACCOUNT`, `HARVEST_CLIENT_ID` and `HARVEST_CLIENT_SECRET`
        - `ASANA_CLIENT_ID` and `ASANA_CLIENT_SECRET`
        - `INFUSIONSOFT_APP_NAME`, `INFUSIONSOFT_CLIENT_ID` and `INFUSIONSOFT_CLIENT_SECRET`
    - Auth files: each of these are a path to a file that will store the auth
    token for each service:
        - `ASANA_AUTH_FILE`
        - `HARVEST_AUTH_FILE`
4. Prepare your web server to receive authenticated responses at `auth/(asana|harvest|infusion).php` ([see #1](https://bitbucket.org/theapiguys/api-helpers/issues/1/))
5. Take a look at the source code and see what each class is capable of :)
