<?php namespace ApiHelpers;
use Harvest\HarvestApi;
use Harvest\Model\Result;

abstract class Harvest {

    /**
     * @param bool $restart Whether this should force a client reinstantiation or simply use what's already available
     * @return HarvestApi
     */
    static function client(bool $restart = false) {
        static $client;
        if (!$client || $restart) {
            $client = (new HarvestApi)
                ->setAccount(HARVEST_ACCOUNT)
                ->setClientId(HARVEST_CLIENT_ID)
                ->setClientSecret(HARVEST_CLIENT_SECRET)
                ->setRedirectUri(BASE_URL.'/auth/harvest.php');

            if (file_exists(HARVEST_AUTH_FILE)) {
                $token = unserialize(file_get_contents(HARVEST_AUTH_FILE));
                if ($token->expires <= time()) {
                    (new RefreshPolicy(RefreshPolicy::HARVEST))(function () {
                        return self::client(true);
                    }, function () use ($client, &$token) {
                        $result = $client->refreshToken($token->refresh_token);
                        $token  = self::storeToken($result->data);
                    });
                }
                $client->setToken($token->access_token);
            }
        }
        return $client;
    }

    /**
     * Token post-processing.
     * Calculates the final expiry date for this token, and stores it at the right place. Their original response is only
     * useful at the moment it's received, as they return the validity period, not the final expiry time, so we must
     * calculate it straight ahead or else we'll only know if the token is expired when it's too late.
     * That could be verified through the file write time, but that doesn't seem _that_ reliable...
     * @param \stdClass $data The 'data' field from what usually comes from the result of a Harvest request
     * @return \stdClass $data The same object, added the "expires" timestamp property
     */
    static function storeToken(\stdClass $data) {
        $data->expires = time() + $data->expires_in;
        file_put_contents(HARVEST_AUTH_FILE, serialize($data));
        return $data;
    }

    /**
     * Syntatic sugar to make it easier to gather any silent Harvest error through a single try..catch block.
     * @example $client_id = harvest_data($harvest->createClient($payload));
     * @param Result $result
     * @return array|mixed
     * @throws \RuntimeException
     */
    static function data(Result $result) {
        if ($result->isSuccess()) {
            return $result->data;
        } else {
            throw new \RuntimeException('Harvest error: '.$result->data);
        }
    }
}