<?php namespace ApiHelpers;

use Infusionsoft\Token;

abstract class Infusionsoft {

    public static $authUri = '/auth/infusion.php';

    /** @var Token */
    protected static $token;
    /** @var callable */
    protected static $refreshFunc;

    /**
     * Sets a custom token to be used - i.e. for session-based usage.
     * @param Token|null    $token   The token object. If set to null, will go back to file-based token
     * @param callable|null $refresh The procedure store the refreshed token, when needed. The signature should be
     *                               function(Token $token):void
     */
    static function setToken(Token $token = null, callable $refresh = null) {
        self::$token = $token;
        self::$refreshFunc = $refresh;
    }

    /**
     * @param bool $restart Whether this should force a client reinstantiation or simply use what's already available
     * @see setToken
     * @return \Infusionsoft\Infusionsoft
     */
    static function client(bool $restart = false) {
        static $client;
        if (!$client || $restart) {
            $client = new \Infusionsoft\Infusionsoft([
                'clientId'     => INFUSIONSOFT_CLIENT_ID,
                'clientSecret' => INFUSIONSOFT_CLIENT_SECRET,
                'redirectUri'  => BASE_URL.self::$authUri,
            ]);

            if (self::$token || file_exists(INFUSIONSOFT_AUTH_FILE)) {
                /** @var Token $token */
                $token = self::$token ?? unserialize(file_get_contents(INFUSIONSOFT_AUTH_FILE));
                $client->setToken($token);

                if ($token->isExpired()) {
                    (new RefreshPolicy(RefreshPolicy::IFS))(
                        function() { return self::client(true); },
                        function() use ($client) {
                            $token = $client->refreshAccessToken();
                            if (self::$token && self::$refreshFunc) {
                                self::$refreshFunc($token);
                            } else {
                                file_put_contents(INFUSIONSOFT_AUTH_FILE, serialize($token));
                            }
                        }
                    );
                }
            }
        }
        return $client;
    }

    /**
     * Syntatic sugar to achieve a goal in Infusionsoft.
     * @param string $goal       Goal key string
     * @param int    $contact_id Contact ID related to this event
     * @param bool   $print      If the result should be printed, besides returning the boolean
     * @return bool
     */
    static function achieveGoal(string $goal, int $contact_id, bool $print = true):bool {
        $res = self::client()->funnels()->achieveGoal(INFUSIONSOFT_APP_NAME, $goal, $contact_id);
        if ($print) {
            echo "IFS goal $goal for $contact_id ".($res[0]['success']? 'triggered' : "failed: {$res[0]['msg']}")."\n";
        }
        return $res[0]['success'];
    }

    /**
     * Sugar for data()->query() infinite parameter list :)
     * @param string    $table  Table name to query for
     * @param int|array $query  an ID to query for, or an array of query parameters
     * @param array     $fields A list of fields to be retrieved
     * @return array The resulting object (not a list of one element).
     */
    static function findOne(string $table, $query, array $fields) {
        if (is_numeric($query)) {
            $query = ['Id' => $query];
        }
        $result = self::client()->data()->query($table, 1, 0, $query, $fields, 'Id', true);
        return $result? $result[0] : null;
    }

    /**
     * Similar to {@link findOne()}, but instead finds a thousand records - the maximum on a page.
     * Mostly useful for development exploration or debugging.
     * @param string    $table  Table name to query for
     * @param int|array $query  an ID to query for, or an array of query parameters
     * @param array     $fields A list of fields to be retrieved
     * @return array The resulting list, directly from data()->query().
     */
    static function find1000(string $table, array $query, array $fields) {
        return self::client()->data()->query($table, 1000, 0, $query, $fields, 'Id', true);
    }

}
