<?php namespace ApiHelpers;

/**
 * Controls refresh token lockfiles (to prevent race conditions) and has an utility method (@link shouldRetry} to tell
 * whether a script should retry the token or just go ahead and refresh it.
 * The fastest usage is through {@link __invoke}; look at its example to see how to use that. Another recommended usage
 * is going through {@link shouldRetry} and {@link refreshWithLock} (basically what {@link __invoke} does).
 *
 * Mostly an internal class, not to be used outside of this library's classes.
 */
class RefreshPolicy {

    const HARVEST = 'harvest';
    const ASANA   = 'asana';
    const IFS     = 'ifs';

    protected $service;
    protected $lockfile;

    /**
     * @param string $service A service name. Use one of the self::HARVEST, self::ASANA or self::IFS constants.
     */
    function __construct(string $service) {
        $this->service = $service;
        $this->lockfile = CACHE_DIR.DIRECTORY_SEPARATOR.$this->service.'.refresh-lock';
    }

    /**
     * Tells whether the refresh token should happen now or wait a bit and try again, avoiding race conditions.
     * Technically, manages lockfiles so concurrent scripts won't try to refresh tokens all at the same time.
     * @return bool If true, proceed; if false, will sleep(5) and return false, meaning "reverify the token file"
     */
    function shouldRetry():bool {
        if (file_exists($this->lockfile)) {
            sleep(5);
            return true;
        } else {
            return false;
        }
    }

    function lock() {
        touch($this->lockfile);
    }

    function unlock() {
        unlink($this->lockfile);
    }

    /**
     * A simple wrapper that makes sure to {@link lock} before the $refresh_procedure, and {@link unlock} afterwards.
     * @param callable $refresh_procedure Whatever you should do to refresh the current token.
     * @return mixed whatever $refresh_procedure returns, if any
     */
    function refreshWithLock(callable $refresh_procedure) {
        $this->lock();
        $result = $refresh_procedure();
        $this->unlock();
        return $result;
    }

    /**
     * Syntatic sugar to make customary usage more standard. See example for a sample implementation.
     * @example
     * return (new RefreshPolicy(ASANA))(
     *     function() use ($force_oauth) {
     *         return asana_client($force_oauth); //re-runs the original function call until the token is found
     *     },
     *     function() use($auth_client, $token) {
     *         $auth_client->dispatcher->refreshToken = $token->refresh_token;
     *         $token_value = $auth_client->dispatcher->refreshAccessToken();
     *         store_asana_token($auth_client->dispatcher);
     *         return Asana\Client::accessToken($token_value);
     *     }
     * );
     *
     * @param callable $retry What should be done if the refresh should be skipped, retrying the token file
     * @param callable $refresh What's to be done if the refresh is authorized
     * @return mixed
     */
    function __invoke(callable $retry, callable $refresh) {
        if ($this->shouldRetry()) {
            return $retry();
        } else {
            return $this->refreshWithLock($refresh);
        }
    }
}