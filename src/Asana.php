<?php namespace ApiHelpers;
use Asana\Client;
use Asana\Dispatcher\OAuthDispatcher;
use Asana\Iterator\ItemIterator;

abstract class Asana {
    /**
     * Returns an OAuth-ready or Token-ready client, depending on the auth state - i.e. if the token is stored correctly.
     * @param bool $force_oauth will force the return of a clean OAuth client.
     * @param bool $restart Whether this should force a client reinstantiation or simply use what's already available
     * @return Client
     */
    static function client(bool $force_oauth = false, bool $restart = false) {
        static $token_client, $auth_client;

        if (!$force_oauth && file_exists(ASANA_AUTH_FILE)) {
            $token = unserialize(file_get_contents(ASANA_AUTH_FILE));
            if ($token->expires > time()) {
                if (!$token_client || $restart) {
                    $token_client = Client::accessToken($token->token);
                }
                return $token_client;
            }
        }

        if (!$auth_client || $restart) {
            $auth_client = Client::oauth([
                'client_id'     => ASANA_CLIENT_ID,
                'client_secret' => ASANA_CLIENT_SECRET,
                'redirect_uri'  => BASE_URL.'/auth/asana.php'
            ]);
        }

        if ($force_oauth) {
            return $auth_client;
        }

        if (isset($token)) { //if we got here it means the token file exists, but is expired. let's refresh, then
            return (new RefreshPolicy(RefreshPolicy::ASANA))(
                function() use ($force_oauth) {
                    return self::client($force_oauth, true);
                },
                function() use($auth_client, $token) {
                    $auth_client->dispatcher->refreshToken = $token->refresh_token;
                    $token_value = $auth_client->dispatcher->refreshAccessToken();
                    self::storeToken($auth_client->dispatcher);
                    return Client::accessToken($token_value);
                }
            );
        }

        return $auth_client;
    }

    /**
     * Gathers all the useful information that is scattered around the Asana Dispatcher object, and calculates the
     * expiration time for the current token. Then, store all that in the auth file.
     * @param OAuthDispatcher $dispatcher
     */
    static function storeToken(OAuthDispatcher $dispatcher) {
        $details = (object)[
            'token' => $dispatcher->accessToken,
            'refresh_token' => $dispatcher->refreshToken,
            'expires' => $dispatcher->expiresIn + time()
        ];
        file_put_contents(ASANA_AUTH_FILE, serialize($details));
    }

    /**
     * Asana Iterators can't count :|
     * @param ItemIterator $iterator
     * @return int
     */
    static function count(ItemIterator $iterator):int {
        $total = 0;
        foreach($iterator as $item) {
            ++$total;
        }
        return $total;
    }
}